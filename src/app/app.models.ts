export interface Product {
  id: number;
  category: ProductCategory; // to be converted to enum
  name: string;
  imageUrl: string;
  price: string;
  description: string;
}

export enum ProductCategory {
  Laptops,
  Smartphones,
  Accesories
}

export const ItemsSessionKey = 'inCart';