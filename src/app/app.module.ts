import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductsListComponent } from './products-list/products-list.component';
import { CartComponent } from './cart/cart.component';
import { ProductsProviderService } from './products-list/product-list.service';
import { HttpClientModule } from '@angular/common/http';
import { SessionManager } from './session-manager.service';
import { QuantityComponent } from './quantity/quantity.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsListComponent,
    CartComponent,
    QuantityComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ProductsProviderService, SessionManager],
  bootstrap: [AppComponent]
})
export class AppModule { }
