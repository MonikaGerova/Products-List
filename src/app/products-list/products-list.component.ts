import { Component, OnInit } from '@angular/core';
import { ItemsSessionKey, Product } from '../app.models';
import { SessionManager } from '../session-manager.service';
import { ProductsProviderService } from './product-list.service';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {
  products: Product[];

  constructor(
    private productsProvider: ProductsProviderService, 
    private sessionManager: SessionManager,
  ) { }

  ngOnInit() {
    this.productsProvider.getProducts().subscribe((data: Product[]) => {
      this.products = data;
    });
  }

  getPrice(price: any): number {
    return price / 100;
  }

  hasQuantityInCart(id:number): boolean {
    return !!this.sessionManager.getItemCountFromSession(ItemsSessionKey, id.toString());
  }

  increaseQuantity(id: number): void {
    this.sessionManager.addItemToCart(ItemsSessionKey, id.toString());
  }
}
