import { Component, OnInit } from '@angular/core';
import { ItemsSessionKey, Product } from '../app.models';
import { ProductsProviderService } from '../products-list/product-list.service';
import { SessionManager } from '../session-manager.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  productsInCart: Product[];
  
  constructor( 
    private productsProvider: ProductsProviderService,
    private sessionManager: SessionManager) { 
  }

  ngOnInit() {
    const itemsIds = [];

    for(let key of this.sessionManager.getCartItems(ItemsSessionKey).keys()) {
      itemsIds.push(key);
    }

    this.productsProvider.getProducts().subscribe((data: Product[]) => {
      this.productsInCart = data.filter(item => itemsIds.includes(item.id.toString()));
    });
  }

  get hasProductsInCart(): boolean {
    return this.productsInCart.length !==0;
  }

  getPrice(id: number, price: any): number {
    const quantity = this.sessionManager.getItemCountFromSession(ItemsSessionKey, id.toString());
    return price*quantity/100;
  }

  removeItem(id:number): void {
    this.sessionManager.removeItemFromCart(ItemsSessionKey, id.toString(), true);
    this.productsInCart = this.productsInCart.filter(product => product.id !== id);
  }

  checkout(): void {
    this.sessionManager.clearSession(ItemsSessionKey);
    this.productsInCart = [];
  }

  get totalPrice(): number {
    let totalPrice = 0;
    this.productsInCart.forEach(product => totalPrice += this.getPrice(product.id, product.price))

    return totalPrice;
  }
}
