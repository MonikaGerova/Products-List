import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ItemsSessionKey } from '../app.models';
import { SessionManager } from '../session-manager.service';

@Component({
  selector: 'app-quantity',
  templateUrl: './quantity.component.html',
  styleUrls: ['./quantity.component.css']
})
export class QuantityComponent {

  @Input() id: number;
  @Input() stopDecrease: boolean;
  @Output() removeItem = new EventEmitter<number>();
  
  constructor( private sessionManager: SessionManager) { }

  increaseQuantity(): void {
    this.sessionManager.addItemToCart(ItemsSessionKey, this.id.toString());
  }

  decreaseQuantity(): void {
    this.sessionManager.removeItemFromCart(ItemsSessionKey, this.id.toString());

    if(!this.quantity) {
      this.removeItem.emit(this.id);
    }
  }

  get quantity(): number {
    return this.sessionManager.getItemCountFromSession(ItemsSessionKey, this.id.toString());
  }
}
