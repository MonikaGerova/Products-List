import { Injectable } from '@angular/core';
import { ItemsSessionKey } from './app.models';

@Injectable()
export class SessionManager {

  constructor() { }

  public getCartItems(item: string): Map<string,number> {
    return this.jsonToMap(localStorage.getItem(item));
  }

  public saveToSession(sessionKey: string, json:  Map<string,number>): void {
    localStorage.setItem(sessionKey, this.mapToJson(json));
  }

  public getItemCountFromSession(sessionKey: string, productId: string): number | null {
    const session = this.getCartItems(sessionKey);
    return session.get(productId);
  }
  
  public getTotalQuantityInCart(sessionKey: string): number {
    let count = 0;
    this.getCartItems(sessionKey).forEach((value) => count += value);
    return count;
  }

  public addItemToCart(item: string, id: string): void {
    let localSession = this.getCartItems(item);

    const increaseProductCount = localSession.get(id)?  localSession.get(id) + 1 : 1;
    localSession.set(id, increaseProductCount);

    this.saveToSession(item, localSession);
  }

  public removeItemFromCart(item: string, id: string, deleteItem: boolean = false): void {
    let localSession = this.getCartItems(item);

    if(deleteItem) {
      localSession.delete(id);
    } else {
      const newCount = localSession.get(id) - 1;
    
      if(newCount == 0) {
        localSession.delete(id);
      } else {
        localSession.set(id, newCount);
      }
    }
    
    this.saveToSession(item, localSession);
  }

  public clearSession(sessionKey: string): void {
    localStorage.removeItem(sessionKey);
  }

  private mapToJson(map) {
    return JSON.stringify([...map]);
  }
  private jsonToMap(jsonStr) {
    return new Map<string,number>(JSON.parse(jsonStr));
  }
}