import { Component } from '@angular/core';
import { ItemsSessionKey } from './app.models';
import { SessionManager } from './session-manager.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Products';

  constructor( private sessionManager: SessionManager) { }

  get totalQuantity(): number {
    return this.sessionManager.getTotalQuantityInCart(ItemsSessionKey);
  }
}
